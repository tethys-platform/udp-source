use chrono::Utc;
use local_ip_address::list_afinet_netifas;
use socket2::{Domain, Protocol, SockAddr, Socket, Type};
use std::env;
use std::error::Error;
use std::net::{IpAddr, SocketAddr, ToSocketAddrs};
use std::process;
use std::time::Duration;
use tokio::{net::UdpSocket, sync::mpsc, time::timeout};

pub mod tethys {
    tonic::include_proto!("tethys"); // The string specified here must match the proto package name
}
use tethys::producer_client::ProducerClient;

#[macro_use]
extern crate log;

mod config;
use config::Config;

#[macro_use]
extern crate lazy_static;

lazy_static! {
    static ref DEBUG: bool = env::var("DEBUG")
        .unwrap_or("false".to_string())
        .parse()
        .expect(
            "$DEBUG produced a parse error; to run in DEBUG mode, it should be set to \"true\""
        );
    static ref STATUS_UPDATE_PACKET_INTERVAL: u64 = env::var("STATUS_UPDATE_PACKET_INTERVAL")
        .unwrap_or("1000".to_string())
        .parse()
        .expect("$STATUS_UPDATE_PACKET_INTERVAL produced a parse error; it should be an integer");
}

static NO_TIMEOUT: u64 = 10_000_000_000u64;

// creates a UDP socket based on config
async fn open_udp(config: &Config) -> Result<UdpSocket, Box<dyn Error>> {
    let address: IpAddr = config.address.parse().expect("address parse error");
    let host_and_port = format!("{}:{}", config.address, config.port);

    let addr = host_and_port
        .to_socket_addrs()
        .expect("Failed to parse address from hostname")
        .next()
        .unwrap();

    let mut local_address: IpAddr = config
        .local_address
        .parse()
        .expect("local_network_address parse error");

    let client = SocketAddr::new(address, config.port);

    let domain = if addr.is_ipv6() {
        Domain::IPV6
    } else {
        Domain::IPV4
    };

    let socket = Socket::new(domain, Type::DGRAM, Some(Protocol::UDP))?;

    // try to set the socket buffer size
    if socket
        .set_recv_buffer_size(config.socket_buffer_size)
        .is_err()
    {
        warn!(
            "Failed to set SO_RCVBUF to {}{}",
            config.socket_buffer_size,
            log_metadata(config)
        );
    }

    local_address = get_local_address(config.local_network_interface.clone(), local_address);

    if address.is_multicast() {
        match (address, local_address) {
            (IpAddr::V4(ref addr_v4), IpAddr::V4(ref localaddr)) => {
                socket.set_multicast_if_v4(localaddr)?;
                socket.join_multicast_v4(addr_v4, localaddr)?;
                info!("Joined multicast IPv4 address {addr_v4} on interface {localaddr}");
            }
            (IpAddr::V6(ref addr_v6), ref _localaddr) => {
                let interface: u32 = get_interface_index(config.local_network_interface.clone());
                socket.set_multicast_if_v6(interface)?;
                socket.join_multicast_v6(addr_v6, interface)?;
                socket.set_only_v6(true)?;
                info!("Joined multicast IPv6 address {addr_v6} on interface {interface}");
            }
            (IpAddr::V4(_), IpAddr::V6(_)) => {
                return Err("local address is IpV6, mca address is IpV4".into());
            }
        }

        socket.set_reuse_address(true)?;
    } else {
        info!("Binding to unicast address {address}")
    }

    if let Err(e) = socket.set_nonblocking(true) {
        warn!("Error trying to set udp socket as nonblocking: {e}");
    }

    socket.bind(&SockAddr::from(client))?;
    let udp_socket = UdpSocket::from_std(socket.into())?;

    Ok(udp_socket)
}

// if a network interface was provided, then try to get the address for it
// if an interface wasn't provided or anything goes wrong, default back to
// the configured local address
fn get_local_address(interface: Option<String>, address: IpAddr) -> IpAddr {
    if let Some(interface) = interface {
        let ifas = list_afinet_netifas();

        if ifas.is_err() {
            warn!("Error getting network interfaces list: {:?}", ifas.err());
            return address;
        }

        let ifas = ifas.unwrap();

        match address {
            IpAddr::V4(_addr) => {
                let found_interface = ifas.iter().find(|(name, ipaddr)| {
                    *name == interface.as_str() && matches!(ipaddr, IpAddr::V4(_))
                });

                if let Some((name, ipaddr)) = found_interface {
                    info!("Using the local address for {name}: {ipaddr:?}");
                    return *ipaddr;
                }
            }
            IpAddr::V6(_addr) => {
                let found_interface = ifas.iter().find(|(name, ipaddr)| {
                    *name == interface.as_str() && matches!(ipaddr, IpAddr::V6(_))
                });

                if let Some((name, ipaddr)) = found_interface {
                    info!("Using the local address for {name}: {ipaddr:?}");
                    return *ipaddr;
                }
            }
        }

        error!("Couldn't find a network interface named {interface}");
    }

    address
}

fn get_interface_index(interface: Option<String>) -> u32 {
    if let Some(interface) = interface {
        let ifas = list_afinet_netifas();

        if ifas.is_err() {
            warn!("Error getting network interfaces list: {:?}", ifas.err());
            return 0;
        }

        let ifas = ifas.unwrap();

        if let Some(index) = ifas
            .iter()
            .position(|(name, _)| *name == interface.as_str())
        {
            index as u32
        } else {
            0
        }
    } else {
        0
    }
}

// compares if two SockAddrs are the same
fn same_addr(a1: &SocketAddr, a2: &SocketAddr) -> bool {
    a1.eq(a2)
}

// appends the config address and port to log messages
fn log_metadata(config: &Config) -> String {
    format!("      address={} port={}", config.address, config.port)
}

async fn start_client_stream(
    channel: tonic::transport::Channel,
    mut rx: mpsc::Receiver<Vec<u8>>,
    config: Config,
) -> Result<(), Box<dyn Error>> {
    let component_id = config.tethys_property_component_id.clone();
    let mut client = ProducerClient::with_interceptor(channel, |mut req: tonic::Request<()>| {
        req.metadata_mut()
            .insert("tethys-component-id", component_id.parse().unwrap());
        Ok(req)
    });

    let outbound = async_stream::stream! {
        let mut buf = vec![];
        let mut batch_size = 0;
        let mut batch_timeout = Utc::now().timestamp_millis() + config.max_batch_timeout;
        let mut timeout_duration = NO_TIMEOUT;
        let mut batch_origin_time = 0;

        // DEBUG status metrics
        let mut update_count = 0u64;
        let mut total_packets = 0u64;
        let mut total_bytes = 0u64;
        let mut batches_sent = 0u64;
        let mut bytes_sent = 0u64;
        let mut batch_timeouts = 0u64;

        loop {
            match timeout(Duration::from_millis(timeout_duration), rx.recv()).await {
                Ok(Some(mut packet)) => {
                    if batch_size == 0 { batch_origin_time = Utc::now().timestamp_nanos(); }
                    if *DEBUG {
                        total_packets += 1;
                        total_bytes += packet.len() as u64;
                    }

                    buf.append(&mut packet);
                    batch_size += 1;

                    let now = Utc::now().timestamp_millis();
                    if batch_size >= config.max_batch_size || now >= batch_timeout {
                        let mut request = tethys::ProduceDataRequest::default();
                        let mut data_file = tethys::ProduceDataFile::default();
                        data_file.content = buf.clone();
                        data_file.origin_time = batch_origin_time;
                        request.data_files.push(data_file);

                        yield request;

                        if *DEBUG {
                            batches_sent += 1;
                            bytes_sent += buf.len() as u64;
                        }

                        buf.clear();
                        buf.shrink_to_fit();
                        batch_size = 0;
                        batch_timeout = now + config.max_batch_timeout;
                        timeout_duration = NO_TIMEOUT;
                    } else {
                        let delta = batch_timeout - now;
                        if delta < 0 {
                            timeout_duration = 0;
                        } else {
                            timeout_duration = delta as u64;
                        }
                    }
                }
                Ok(None) => (),
                Err(_) => {
                    if batch_size > 0 {
                        let mut request = tethys::ProduceDataRequest::default();
                        let mut data_file = tethys::ProduceDataFile::default();
                        data_file.content = buf.clone();
                        data_file.origin_time = batch_origin_time;
                        request.data_files.push(data_file);

                        yield request;

                        if *DEBUG {
                            batch_timeouts += 1;
                            batches_sent += 1;
                            bytes_sent += buf.len() as u64;
                        }

                        buf.clear();
                        buf.shrink_to_fit();
                        batch_size = 0;
                        batch_timeout = Utc::now().timestamp_millis() + config.max_batch_timeout;
                        timeout_duration = NO_TIMEOUT;
                    }
                },
            }

            if *DEBUG && (total_packets % *STATUS_UPDATE_PACKET_INTERVAL == 0) {
                update_count += 1;
                debug!("status update {update_count}:  packets_received={total_packets}  bytes_received={total_bytes}  batches_sent={batches_sent}  bytes_sent={bytes_sent}  batch_timeouts={batch_timeouts}");
            }
        }
    };

    let _response = client
        .produce_data_stream(tonic::Request::new(outbound))
        .await?;

    Ok(())
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    let env = env_logger::Env::default()
        .filter_or(
            "LOG_LEVEL",
            if *DEBUG { "udp_source=DEBUG" } else { "INFO" },
        )
        .write_style_or("LOG_COLORS", "never");
    env_logger::Builder::from_env(env).init();

    info!("Starting udp-source");

    let config: Config = config::load_config();
    let config_clone = config.clone();

    let agent_address = format!("http://{}", config.tethys_property_agent_address.clone());

    use std::time::Duration;
    use tonic::transport::Endpoint;
    let builder = Endpoint::from_shared(agent_address.clone())?
        .connect_timeout(Duration::from_secs(config.agent_connect_timeout_seconds));
    let channel = builder.connect().await?;

    info!("Connected to tethys-agent @ {agent_address}");

    let (tx, rx) = mpsc::channel(1_000);
    tokio::spawn(async move {
        let result = start_client_stream(channel, rx, config_clone);

        match result.await {
            Ok(()) => error!("Unexpected return from start_client_stream"),
            Err(e) => error!("Err result from start_client_stream: {:?}", e),
        };

        process::exit(1);
    });

    let udp_socket = open_udp(&config).await?;

    // create receive buffer based on config
    let mut buf = vec![0u8; config.receive_buffer_size];

    let mut packet_source = None;
    let mut bytes_received: u64 = 0;
    let mut packets_received: u64 = 0;

    loop {
        match udp_socket.recv_from(&mut buf).await {
            Ok((len, remote_addr)) => {
                match tx.send(buf[0..len].to_vec()).await {
                    Ok(_) => (),
                    Err(e) => {
                        error!("Error sending packet to channel: {e}");
                    }
                };

                bytes_received += len as u64;
                packets_received += 1;

                if packets_received % 10_000 == 0 {
                    debug!("Status totals  packets={packets_received}  bytes={bytes_received}");
                }

                // log if the source address has changed since the last message
                if let Some(addr) = packet_source {
                    if !same_addr(&addr, &remote_addr) {
                        debug!(
                            "Source address changed from {:?} to {:?}{}",
                            &addr,
                            &remote_addr,
                            log_metadata(&config)
                        )
                    }
                }

                packet_source = Some(remote_addr);
            }
            Err(e) => {
                panic!("UDP recv error, exiting: {}{}", e, log_metadata(&config));
            }
        }
    }
}
