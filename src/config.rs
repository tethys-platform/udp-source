use serde::Deserialize;

#[cfg(debug_assertions)]
use dotenv::dotenv;

// default AGENT_CONNECT_TIMEOUT_SECONDS
fn default_agent_connect_timeout_seconds() -> u64 {
    5
}

// default LOCAL_NETWORK_INTERFACE
fn default_local_address() -> String {
    "0.0.0.0".to_string()
}

// default LOCAL_NETWORK_INTERFACE
fn default_local_network_interface() -> Option<String> {
    None
}

// default MAX_BATCH_SIZE
fn default_max_batch_size() -> usize {
    1
}

// default MAX_BATCH_TIMEOUT
fn default_max_batch_timeout() -> i64 {
    1_000 // milliseconds
}

// default RECEIVE_BUFFER_SIZE
fn default_receive_buffer_size() -> usize {
    65_507 // bytes
}

// default SOCKET_BUFFER_SIZE
fn default_socket_buffer_size() -> usize {
    1_024 * 1_024 // 1MB
}

#[derive(Clone, Deserialize, Debug)]
pub struct Config {
    // required
    pub address: String,
    pub port: u16,

    // provided by Tethys platform
    pub tethys_property_component_id: String,
    pub tethys_property_agent_address: String,

    // optional
    #[serde(default = "default_agent_connect_timeout_seconds")]
    pub agent_connect_timeout_seconds: u64,
    #[serde(default = "default_local_address")]
    pub local_address: String,
    #[serde(default = "default_local_network_interface")]
    pub local_network_interface: Option<String>,
    #[serde(default = "default_max_batch_size")]
    pub max_batch_size: usize,
    #[serde(default = "default_max_batch_timeout")]
    pub max_batch_timeout: i64,
    #[serde(default = "default_receive_buffer_size")]
    pub receive_buffer_size: usize,
    #[serde(default = "default_socket_buffer_size")]
    pub socket_buffer_size: usize,
}

pub fn load_config() -> Config {
    load_environment_variables();

    match envy::from_env::<Config>() {
        Ok(config) => {
            debug!("{:#?}", config);
            config
        }
        Err(error) => panic!("{error:#?}"),
    }
}

// load from .env file in development
#[cfg(debug_assertions)]
fn load_environment_variables() {
    dotenv::from_filename(".env.develop").ok();
    dotenv().ok();
}

// noop for production
#[cfg(not(debug_assertions))]
fn load_environment_variables() {}
