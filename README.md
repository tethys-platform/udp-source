# udp-source

A Tethys source processor for subscribing to unicast or multicast UDP traffic and producing the data into a pipeline.

## Tethys Config

There are several required and optional configuration properties for running `udp-source` as part of a Tethys pipeline.

### Required Tethys Config
---
#### `ADDRESS`
`string`

The UDP address.

#### **`PORT`**
`integer`

The UDP port.

#### **`TETHYS_PROPERTY_AGENT_ADDRESS`**
`string`

The host:port of the pipeline's agent.

Provided by the Tethys platform in production scenarios.

Example:
    
    localhost:5000

#### **`TETHYS_PROPERTY_COMPONENT_ID`**
`string`

The unique ID for this processor within this pipeline. This is passed to the agent on connection to identify the source.

Provided by the Tethys platform in production scenarios.

Example:

    ABC123

> If you are deploying either a pipeline either via JSON-config or Docker Compose, you will need to ensure this value matches the component setting. If you are deploying multiple pipelines, this value is only required to be unique **within a single pipeline**, so reusing the same value across pipelines is ok.

### Optional Tethys Config
---
#### `AGENT_CONNECT_TIMEOUT_SECONDS`
`integer`

Default: `5`

The number of seconds to wait on an attempted connection to the Tethys agent before timing out.

#### `LOCAL_ADDRESS`
`string`

Default: `0.0.0.0`

The local IP address on which to listen for incoming packets.

> If `LOCAL_NETWORK_INTERFACE` is set, then that value will take precedence over this one. In the event that the interface can't be found, the service will fallback to this setting.

#### `LOCAL_NETWORK_INTERFACE`
`string`

The local network interface on which to listen for incoming packets. If set, the service will try to find the local address for the interface. If the interface isn't found, the service will fallback to using the `LOCAL_ADDRESS` setting.

Example:

    net1

#### **`MAX_BATCH_SIZE`**
`integer`

Default: `1`

The maximum number of messages to batch into a single message data file. When this number is reached, a request will be sent to the Tethys agent containing all of the batched data. If `MAX_BATCH_TIMEOUT` is reached before this number, the request will be sent and the batch size will be reset to 0.

> Currently all data is batched into a single byte array. If you're processing data where this behavior isn't usable, keep the batch size at 0. The source will be enhanced in 0.6.0 to support batching separate data files as part of a single request to the agent.

#### **`MAX_BATCH_TIMEOUT`**
`integer`

Default: `1000` (milliseconds)

The maximum time to wait, in milliseconds, before sending data to the Tethys agent. When this value is reached, and there is batched data, the data will be sent to the Tethys agent in a single request. If `MAX_BATCH_SIZE` is reached before the timeout, then the request will be sent and the timeout will be canceled.

#### **`RECEIVE_BUFFER_SIZE`**
`integer`

Default: `65507` (bytes)

The size of the buffer, in bytes, used to receive UDP packet data. This size can be adjusted based on the expected size of incoming data.

#### **`SOCKET_BUFFER_SIZE`**
`integer`

Default: `1048576` (bytes)

The maximum size, in bytes, of the socket buffer that should be used. This is a suggestion to the operating system to indicate how big the socket buffer should be. If this value is set too low, the buffer may fill up before the data can be read, and incoming data will be dropped.

## General Config

#### **`DEBUG`**
`bool`

Default: `false`

Run `udp-source` in `DEBUG` mode to see debug messages and print out status updates.

#### **`LOG_LEVEL`**
Type: `string`

Default: `INFO`

The service log level, which will filter out any messages below the level.

> If you want to view `DEBUG` messages from the service, you don't need to change this setting -- `$DEBUG` turns that on automatically. However, it also filters out underlying library debug statements. If you want to *also* view those, set `LOG_LEVEL=DEBUG`.

Review [env_logger](https://docs.rs/log/latest/log/enum.Level.html) Rust crate for possible settings.

#### **`LOG_COLORS`**
Type: `string`

Default: `never`

Enable color styling of log messages. By default, colors are disabled. To enable, set the value to `auto` or `always`. Review [env_logger](https://docs.rs/env_logger/0.9.0/env_logger/index.html#disabling-colors) Rust crate for more information.

#### **`STATUS_UPDATE_PACKET_INTERVAL`**
`integer`

Default: `1000`

When running in `DEBUG` mode, the received packet interval for printing status updates. The updates include packets and bytes received, and bytes and batches sent to the agent.
